﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ParkingOefeningZelf
{
    public partial class Auto
    {
        public override string ToString()
        {
            MessageBox.Show( Nummerplaat + " " + Parkeerplaats.ParkingCode );
            return "De auto met nummerplaat " + Nummerplaat + " is binnengereden om " + TijdstipInkom + " op parkeerplaats " + Parkeerplaats.ParkingCode + " en vertrokken om " + TijdstipVertrek + ".";
        }
    }
}
