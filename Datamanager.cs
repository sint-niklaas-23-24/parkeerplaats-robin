﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingOefeningZelf
{
    public static class Datamanager
    {
        public static List<Parkeerplaats> GetParkeerplaatsen()
        {
            using (var x = new ParkingDBEntities())
            {
                return x.Parkeerplaats.ToList();
            }
        }

        public static List<Auto> GetVertrokkenAutos()
        {
            using (var x = new ParkingDBEntities())
            {
                var query = from autos in x.Auto
                            where autos.TijdstipVertrek != null
                            select autos;

                return query.ToList();
            }

        }

        public static Parkeerplaats GetParkeerplaats(int eenParkeerplaatsID)
        {
            using (var x = new ParkingDBEntities())
            {
                var query = from parkeerplaatsen in x.Parkeerplaats
                            where parkeerplaatsen.ParkeerplaatsID == eenParkeerplaatsID
                            select parkeerplaatsen;

                return query.FirstOrDefault();
            }
        }

        public static Parkeerplaats GetVrijeParkeerplaats()
        {
            using (var x = new ParkingDBEntities())
            {
                var query = from parkeerplaatsen in x.Parkeerplaats
                            where parkeerplaatsen.Beschikbaar == true
                            select parkeerplaatsen;

                return query.FirstOrDefault();
            }
        }

        public static bool UpdateParkeerplaats(Parkeerplaats eenParkeerplaats)
        {
            bool updateSucceeded = false;
            using (var x = new ParkingDBEntities())
            {
                x.Entry(eenParkeerplaats).State = System.Data.Entity.EntityState.Modified;
                if (0 < x.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }
        public static bool UpdateAuto(Auto eenAuto)
        {
            bool updateSucceeded = false;
            using (var x = new ParkingDBEntities())
            {
                x.Entry(eenAuto).State = System.Data.Entity.EntityState.Modified;
                if (0 < x.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        public static Auto GetAuto(int eenParkingID)
        {
            using (var x = new ParkingDBEntities())
            {
                var query = from autos in x.Auto
                            where autos.FKParking == eenParkingID
                            select autos;

                return query.FirstOrDefault();
            }
        }

        public static bool DeleteAuto(Auto eenAuto)
        {
            bool deleteSucceeded = false;
            using (var x = new ParkingDBEntities())
            {
                x.Entry(eenAuto).State = System.Data.Entity.EntityState.Deleted;
                if (0 < x.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }
            }
            return deleteSucceeded;
        }

        public static bool InsertAuto(Auto eenAuto)
        {
            bool geparkeerd = false;
            using (var x = new ParkingDBEntities())
            {
                x.Auto.Add(eenAuto);
                if (0 < x.SaveChanges())
                {
                    geparkeerd = true;
                }
            }
            return geparkeerd;
        }

        public static bool InsertParkeerplaats(Parkeerplaats eenParkeerplaats)
        {
            bool parkingAdded = false;
            using (var x = new ParkingDBEntities())
            {
                x.Parkeerplaats.Add(eenParkeerplaats);
                if (0 < x.SaveChanges())
                {
                    parkingAdded = true;
                }
            }
            return parkingAdded;
        }
    }
}
