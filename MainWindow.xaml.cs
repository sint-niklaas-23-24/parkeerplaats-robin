﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ParkingOefeningZelf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        bool[,,] parking = new bool[3, 5, 5];
        Random mijnRandom = new Random();
        int autosNaarBinnen;
        int autosNaarBuiten;
        int autosGeparkeerd = 0;
        int plaatsenBeschikbaar = 0;
        List<Parkeerplaats> lstParkeerplaatsen = new List<Parkeerplaats>();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            lstParkeerplaatsen = Datamanager.GetParkeerplaatsen();

            //Eerste opvulling van parking tabel in DB

            //for (int i = 0; i < parking.GetLength(0); i++)
            //{
            //    for (int j = 0; j < parking.GetLength(1); j++)
            //    {
            //        for (int k = 0; k < parking.GetLength(2); k++)
            //        {
            //            Parkeerplaats par = new Parkeerplaats();
            //            par.Beschikbaar = true;
            //            par.ParkingCode = $"{i + 1}.{j + 1}.{k + 1}";
            //            Datamanager.InsertParkeerplaats(par);
            //        }
            //    }
            //}

            //Beschikbare plaatsen en aantal autos tellen
            foreach (Parkeerplaats par in lstParkeerplaatsen)
            {
                if (par.Beschikbaar == true)
                {
                    plaatsenBeschikbaar++;
                }
                else
                {
                    autosGeparkeerd++;
                }
            }
            RefreshParkingLabels();
        }
        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            //autos naar binnen
            if (autosNaarBinnen <= plaatsenBeschikbaar)
            {
                for (int i = 0; i < autosNaarBinnen; i++)
                {
                    Auto auto = new Auto();
                    DateTime dateTime = DateTime.Now;
                    auto.TijdstipInkom = dateTime;
                    auto.Nummerplaat = RandomNummerplaat();

                    int randomPlaats = mijnRandom.Next(1, 75);
                    while (Datamanager.GetParkeerplaats(randomPlaats).Beschikbaar == false)
                    {
                        randomPlaats = mijnRandom.Next(1, 75);
                    }
                    auto.FKParking = randomPlaats;
                    Parkeerplaats parkeerplaats = Datamanager.GetParkeerplaats(randomPlaats);
                    //parkeerplaats.Beschikbaar = false;
                    auto.Parkeerplaats = Datamanager.GetParkeerplaats(randomPlaats);
                    auto.Parkeerplaats.Beschikbaar = false;
                    Datamanager.UpdateParkeerplaats(parkeerplaats);
                    Datamanager.InsertAuto(auto);
                    autosGeparkeerd++;
                    plaatsenBeschikbaar--;
                }
            }
            else
            {
                MessageBox.Show("Het aantal autos dat naar binnen wil is groter dan het aantal beschikbare plaatsen. Er zullen er een paar moeten vertrekken.", "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
                for (int i = 0; i < plaatsenBeschikbaar; i++)
                {
                    Auto auto = new Auto();
                    DateTime dateTime = DateTime.Now;
                    auto.TijdstipInkom = dateTime;
                    auto.Nummerplaat = RandomNummerplaat();

                    int randomPlaats = mijnRandom.Next(1, 75);
                    while (Datamanager.GetParkeerplaats(randomPlaats).Beschikbaar == false)
                    {
                        randomPlaats = mijnRandom.Next(1, 75);
                    }
                    auto.FKParking = randomPlaats;
                    Parkeerplaats parkeerplaats = Datamanager.GetParkeerplaats(randomPlaats);
                    //parkeerplaats.Beschikbaar = false;
                    auto.Parkeerplaats = Datamanager.GetParkeerplaats(randomPlaats);
                    auto.Parkeerplaats.Beschikbaar = false;
                    Datamanager.UpdateParkeerplaats(parkeerplaats);
                    Datamanager.InsertAuto(auto);
                }
            }

            //autos naar buiten
            for (int i = 0; i < autosNaarBuiten; i++)
            {

                int parkingID = mijnRandom.Next(1, 75);
                Auto auto1 = Datamanager.GetAuto(parkingID);
                while (auto1.Parkeerplaats.Beschikbaar == true)
                {
                    parkingID = mijnRandom.Next(1, 75);
                    auto1.Parkeerplaats = Datamanager.GetParkeerplaats(parkingID);
                }
                auto1.TijdstipVertrek = DateTime.Now;
                auto1.Parkeerplaats.Beschikbaar = true;
                Parkeerplaats parkeerplaats = Datamanager.GetParkeerplaats(parkingID);
                Datamanager.UpdateAuto(auto1);
                Datamanager.UpdateParkeerplaats(parkeerplaats);
                autosGeparkeerd--;
                plaatsenBeschikbaar++;
            }
            RefreshParkingLabels();
        }
        private string RandomNummerplaat()
        {
            string nummerplaat = "";

            for (int i = 0; i < 3; i++)
            {
                int index = mijnRandom.Next(0, 25);
                char[] alfabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
                nummerplaat += alfabet[index];
            }

            nummerplaat += " - ";

            for (int i = 0; i < 3; i++)
            {
                nummerplaat += mijnRandom.Next(0, 9).ToString();
            }

            return nummerplaat;
        }
        private void RefreshParkingLabels()
        {

            lblVerdiep1.Content = "";
            lblVerdiep2.Content = "";
            lblVerdiep3.Content = "";

            autosNaarBinnen = mijnRandom.Next(1, 10);
            lstParkeerplaatsen = Datamanager.GetParkeerplaatsen();

            //kijken of parking open is en array aanpassen naargelang positie
            foreach (Parkeerplaats par in lstParkeerplaatsen)
            {
                int[] positie = new int[3];
                positie[0] = Convert.ToInt32(par.ParkingCode.Split('.')[0]) - 1;
                positie[1] = Convert.ToInt32(par.ParkingCode.Split('.')[1]) - 1;
                positie[2] = Convert.ToInt32(par.ParkingCode.Split('.')[2]) - 1;

                if (par.Beschikbaar == true)
                {
                    parking[positie[0], positie[1], positie[2]] = false;
                }
                else
                {
                    parking[positie[0], positie[1], positie[2]] = true;
                }
            }

            //arrays printen
            for (int i = 0; i < parking.GetLength(0); i++)
            {
                for (int j = 0; j < parking.GetLength(1); j++)
                {
                    for (int k = 0; k < parking.GetLength(2); k++)
                    {
                        if (i == 0)
                        {
                            if (parking[i, j, k] == true)
                            {
                                lblVerdiep1.Content += "|▮|".PadLeft(5);
                            }
                            else
                            {
                                lblVerdiep1.Content += "|▯|".PadLeft(5);
                            }
                        }
                        else if (i == 1)
                        {
                            if (parking[i, j, k] == true)
                            {
                                lblVerdiep2.Content += "|▮|".PadLeft(5);
                            }
                            else
                            {
                                lblVerdiep2.Content += "|▯|".PadLeft(5);
                            }
                        }
                        else
                        {
                            if (parking[i, j, k] == true)
                            {
                                lblVerdiep3.Content += "|▮|".PadLeft(5);
                            }
                            else
                            {
                                lblVerdiep3.Content += "|▯|".PadLeft(5);
                            }
                        }
                    }
                    if (i == 0)
                    {
                        lblVerdiep1.Content += Environment.NewLine;
                    }
                    else if (i == 1)
                    {
                        lblVerdiep2.Content += Environment.NewLine;
                    }
                    else
                    {
                        lblVerdiep3.Content += Environment.NewLine;
                    }
                }
            }
            autosNaarBinnen = mijnRandom.Next(1, 10);
            autosNaarBuiten = mijnRandom.Next(0, 10);
            while (autosNaarBuiten > autosGeparkeerd)
            {
                autosNaarBuiten = mijnRandom.Next(0, 10);
            }

            lblAutosNaarBinnen.Content = "Er willen " + autosNaarBinnen + " autos naar binnen." + Environment.NewLine;
            lblAutosNaarBinnen.Content += "Er willen " + autosNaarBuiten + " autos naar buiten." + Environment.NewLine;
            Parkeerplaats parkeerSugestie = Datamanager.GetVrijeParkeerplaats();
            lblAutosNaarBinnen.Content += "Parkeersuggestie: " + parkeerSugestie.ParkingCode;
        }
        private void btnUitvoeren_Click(object sender, RoutedEventArgs e)
        {
            switch (cmbKeuze.SelectedIndex)
            {
                case 5:
                    using (StreamWriter sw = new StreamWriter("Rapport.txt"))
                    {
                        List<Auto> lstVertrokkenAutos = new List<Auto>();
                        lstVertrokkenAutos = Datamanager.GetVertrokkenAutos();

                        foreach(Auto auto in lstVertrokkenAutos)
                        {
                            sw.WriteLine(auto.ToString());
                        }
                    }
                    break;
            }
        }
    }
}
